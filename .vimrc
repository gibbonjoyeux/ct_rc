""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" BASICS
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set	tabstop=4
set shiftwidth=4
set colorcolumn=81
set	autoindent
set	smartindent

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" KEY BINDINGS
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let mapleader = "\<Space>"
""" FILE
nnoremap <leader>w		:w <CR>
""" SPLIT WINDOW
nnoremap <leader>sp		:sp <CR> <C-w>w <CR> :e 
nnoremap <leader>vsp	:vsp <CR> <C-w>w <CR> :e 
""" LAUNCH COMMAND
nnoremap <leader>xx		:!
nnoremap <leader>xr		:r!
nnoremap <leader>xf		:%!
""" ANT
nnoremap <leader>ant	:!ant 
nnoremap <leader>ar		:!ant run
nnoremap <leader>ab		:!ant build <CR>
nnoremap <leader>ac		:!ant clean <CR>
nnoremap <leader>as		:!ant state <CR>
""" VIMR
nnoremap <leader>rcf	:r!vimr C fun 
nnoremap <leader>rcc	:r!vimr C com
